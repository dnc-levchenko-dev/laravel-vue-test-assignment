<?php

declare(strict_types=1);

namespace Src\Contracts;

interface SmsServiceInterface
{
    public function send(string $to, string $from, string $message): void;
}
