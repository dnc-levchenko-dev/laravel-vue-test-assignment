<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email'        => 'required|email:rfc,dns|unique:users,email',
            'name'         => 'required',
            'phone_number' => 'required',
            'country'      => 'required|exists:countries,name'
        ];
    }

    public function getName(): string
    {
        return $this->get('name');
    }

    public function getEmail(): string
    {
        return $this->get('email');
    }

    public function getCountry(): string
    {
        return $this->get('country');
    }

    public function getPhoneNumber(): string
    {
        return $this->get('phone_number');
    }
}
