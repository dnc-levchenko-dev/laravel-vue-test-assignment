<?php

namespace App\Http\Resources;

use App\Models\Country;
use App\Models\PhoneBook;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var Country $country */
        $country = $this->country()->first();

        /** @var PhoneBook $phoneBookData */
        $phoneBookData = $this->number()->first();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'country' => [
                'id' => $country->getId(),
                'name' => $country->getName(),
                'flag' => $country->getFlag(),
            ],
            'phoneNumber' => $country->getIddWithPlus() . $phoneBookData->getNumber()
        ];
    }
}
