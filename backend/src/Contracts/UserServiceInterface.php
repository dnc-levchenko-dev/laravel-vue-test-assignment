<?php

namespace Src\Contracts;

use App\Models\User;
use Src\DTOs\RegisterUserDTO;

interface UserServiceInterface
{
    public function create(RegisterUserDTO $userDTO): User;
}
