<?php

declare(strict_types=1);

namespace Src\Services;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Src\Contracts\CountryRepositoryInterface;
use Src\Contracts\UserServiceInterface;
use Src\DTOs\RegisterUserDTO;

class UserService implements UserServiceInterface
{
    const DEFAULT_PASSWORD = 'password';

    private CountryRepositoryInterface $countryRepository;

    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @throws \Exception
     */
    public function create(RegisterUserDTO $userDTO): User
    {
        DB::beginTransaction();

        try {
            $country = $this->countryRepository->findByName($userDTO->getCountryName());

            $user = User::create([
                'name' => $userDTO->getName(),
                'email' => $userDTO->getEmail(),
                'password' => Hash::make(self::DEFAULT_PASSWORD)
            ]);

            $user->country()->attach($country->getId());
            $user->number()->create(['number' => $userDTO->getPhoneNumber()]);
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();

            throw $exception;
        } finally {
            DB::commit();
            Log::info("User with id={$user->getId()} has been stored successfully");

            return $user;
        }
    }
}
