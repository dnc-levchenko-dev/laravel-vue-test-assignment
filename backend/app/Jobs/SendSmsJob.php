<?php

namespace App\Jobs;

use App\Models\Country;
use App\Models\PhoneBook;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Src\Contracts\SmsServiceInterface;

class SendSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SmsServiceInterface $smsService)
    {
        // this is a temp/simple solution just for demonstration
        if (App::isLocal()) {
            Log::info("SMS has been sent");

            return;
        }

        try {
            /** @var Country $country */
            $country = $this->user->country()->first();

            /** @var PhoneBook $phoneBookModel */
            $phoneBookModel = $this->user->number()->first();

            $smsService->send(
                $country->getIdd() . $phoneBookModel->getNumber(),
                config('vonage.from'),
                $this->buildMessage($this->user->getName())
            );
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }

    private function buildMessage(string $userName): string
    {
        return "Congratulations Mr. $userName, you've been successfully registered";
    }
}
