<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesSeeder extends Seeder
{
    const FILENAME = 'countries.json';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countriesJson = file_get_contents(storage_path(self::FILENAME), true);

        foreach (json_decode($countriesJson, true) as $country) {
            DB::table('countries')->updateOrInsert([
                'name' => $country['name'],
                'flag' => utf8_encode($country['flag']),
                'idd' => substr($country['idd'], 1),
            ]);
        }
    }
}
