<?php

namespace Src\Contracts;

use App\Models\Country;

interface CountryRepositoryInterface
{
    public function findByName(string $name): Country;
}
