<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Src\Contracts\CountryRepositoryInterface;
use Src\Repositories\Eloquent\CountryRepository;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CountryRepositoryInterface::class, CountryRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
