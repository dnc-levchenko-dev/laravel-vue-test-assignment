<?php

return [
    'apiKey' => env('VONAGE_API_KEY'),
    'apiSecret' => env('VONAGE_API_SECRET'),
    'from' => env('VONAGE_SMS_FROM'),
];
