<?php

namespace Src\Repositories\Eloquent;

use App\Models\Country;
use Src\Contracts\CountryRepositoryInterface;

class CountryRepository implements CountryRepositoryInterface
{
    public function findByName(string $name): Country
    {
        return Country::where('name', 'like', $name)->firstOrFail();
    }
}
