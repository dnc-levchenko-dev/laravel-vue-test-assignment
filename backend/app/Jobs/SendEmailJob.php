<?php

namespace App\Jobs;

use App\Mail\TestEmail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private User $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // this is a temp/simple solution just for demonstration
        if (App::isLocal()) {
            Log::info("Email has been sent to {$this->user->getEmail()}");

            return;
        }

        try {
            Mail::to($this->user->getEmail())->send(new TestEmail($this->user->getName()));
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }
}
