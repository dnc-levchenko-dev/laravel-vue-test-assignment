run `cp .env.example .env`

run `php artisan key:generate`

run `composer install` to install dependencies

set credentials for:

`DB_DATABASE=`

`DB_USERNAME=`

`DB_PASSWORD=`

`MAIL_USERNAME`

`MAIL_PASSWORD`

`VONAGE_API_KEY`

`VONAGE_API_SECRET`

run `php artisan migrate` 

run `php artisan db:seed` 

run `php artisan optimize` 

run `php artisan serve` 

run `php artisan queue:work`

check `laravel.log` after submitting the form
