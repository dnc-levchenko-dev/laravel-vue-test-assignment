<?php

namespace Src\DTOs;

class RegisterUserDTO
{
    private string $name;
    private string $countryName;
    private string $email;
    private string $phoneNumber;

    public static function transform(
        string $name,
        string $countryName,
        string $email,
        string $phoneNumber
    ): self {
        $dto = new self();
        $dto->name = $name;
        $dto->email = $email;
        $dto->countryName = $countryName;
        $dto->phoneNumber = $phoneNumber;

        return $dto;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCountryName(): string
    {
        return $this->countryName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }
}
