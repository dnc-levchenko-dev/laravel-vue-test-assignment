<?php

declare(strict_types=1);

namespace Src\Services;

use Src\Contracts\SmsServiceInterface;
use Vonage\Client;
use Vonage\SMS\Message\SMS;

class SmsService implements SmsServiceInterface
{
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function send(string $to, string $from, string $message): void
    {
        $response = $this->client->sms()->send(
            new SMS($to, $from, $message)
        );

        $message = $response->current();

        if ($message->getStatus() == 0) {
            // do something else according to your ongoing business logic
        } else {
            //log error or retry
        }
    }
}
