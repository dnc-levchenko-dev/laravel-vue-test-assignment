<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'flag',
        'idd',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_country');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getIdd(): int
    {
        return $this->idd;
    }

    public function getIddWithPlus(): string
    {
        return '+' . $this->idd;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFlag(): string
    {
        return utf8_decode($this->flag);
    }
}
