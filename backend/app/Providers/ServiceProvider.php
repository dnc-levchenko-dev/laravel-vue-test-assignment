<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Src\Contracts\SmsServiceInterface;
use Src\Contracts\UserServiceInterface;
use Src\Services\SmsService;
use Src\Services\UserService;
use Vonage\Client\Credentials\Basic;
use Vonage\Client;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserServiceInterface::class, UserService::class);

        $this->app->bind(SmsServiceInterface::class, function ($app) {
            return new SmsService(
                new Client(
                    new Basic(
                        config('vonage.apiKey'),
                        config('vonage.apiSecret')
                    )
                )
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
