<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Resources\UserResource;

use Illuminate\Http\JsonResponse;
use Src\Contracts\UserServiceInterface;
use Src\DTOs\RegisterUserDTO;

class RegisterController extends Controller
{
    private UserServiceInterface $userService;

    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle the incoming request.
     *
     * @param RegisterUserRequest $request
     * @return JsonResponse|UserResource
     */
    public function __invoke(RegisterUserRequest $request): JsonResponse|UserResource
    {
        try {
            $user = $this->userService->create(
                RegisterUserDTO::transform(
                    $request->getName(),
                    $request->getCountry(),
                    $request->getEmail(),
                    $request->getPhoneNumber()
                )
            );
        } catch (\Exception $exception) {
            return response()->json([], $exception->getCode());
        } finally {
            event(new UserRegistered($user));

            return new UserResource($user);
        }
    }
}
